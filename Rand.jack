/**
 * Primitive linear congruential pseudorandom number generator. The LCG
 * algorithm relies on the modulo operator, which is not available in Jack. Use
 * the fact that for any power of 2 n, x mod n is x & (n - 1). The modulo used
 * is 32767, which is 2^15 - 1.
 *
 * Copyright 2022 Jose Quinteiro
 */
class Rand {
    // Initial value. Anything larger than 0 will do
    field int randVal;

    /**
     * Create a new pseudo-random number generator. The seed must be greater
     * than 0.
     */
    constructor Rand new(int p_seed) {
        let randVal = p_seed;
        return this;
    }

    /**
     * Return the next pseudorandom number in this sequence.
     */
    method int next() {
        // These parameters were found empirically. Other attempts below
        // let randVal = ((randVal * 7) + 3) & 32767; // works
        // let randVal = ((randVal * 8) + 9) & 32767; // Stack overflow
        // let randVal = ((randVal * 8) + 3) & 32767; // Stack overflow
        let randVal = ((randVal * 11) + 9) & 32767; // works
        // let randVal = ((randVal * 75) + 74) & 32767; // works
        return randVal;
    }

    /** Deallocates the object's memory. */
    method void dispose() {
        do Memory.deAlloc(this);
        return;
    }
}